<?php

/**
 * 商品接口
 */
$app->group(
    [
        'prefix' => 'mall',
        'namespace' => 'Mall'
    ],
    function () use ($app) {

        $app->post('category/index', 'CategoryController@index'); # 商品分类
        $app->post('index', 'ProductController@index'); # 商品列表
        $app->post('show', 'ProductController@show'); # 商品详细信息

    }
);

/**
 * 订单接口
 */
$app->group(
    [
        'prefix' => 'mall/order',
        'namespace' => 'Mall'
    ],
    function () use ($app) {

        $app->post('store', 'OrderController@store'); # 商品购买下单
        $app->post('member/index', 'OrderMemberController@index'); # 会员订单列表
        $app->post('member/show', 'OrderMemberController@show'); # 会员订单详细
        $app->post('virtual/callback/{mp}', 'OrderCallbackController@virtualCallback'); # 订单虚拟商品回调函数

    }
);