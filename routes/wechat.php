<?php

/**
 * 微信公众号接口
 */
$app->group(
    [
        'prefix' => 'wechat',
        'namespace' => 'Wechat',
        'middleware' => 'wechat'
    ],
    function () use ($app) {

        //$app->get('receive', 'ReceiveController@check');  # 验证微信第三方服务器地址接口
        $app->post('receive', 'ReceiveController@index');  # 接收推送处理接口

        # 菜单管理
        $app->get('menu/index', 'MenuController@index'); # 获取自定义菜单
        $app->get('menu/store', 'MenuController@store'); # 创建自定义菜单

        # 媒体素材管理
        $app->get('media/count', 'MediaController@count'); # 媒体素材总数
        $app->get('media/index', 'MediaController@index'); # 媒体素材列表

        # oauth 身份验证
        $app->get('oauth/code', 'OauthController@code'); # 获取 snsapi 授权链接
        $app->get('oauth/user', 'OauthController@user'); # code回调，拉取用户信息

        # 生成二维码
        $app->get('qr', 'QRCodeController@scene'); # 生成带参数临时二维码

        # 分享接口
        $app->post('jssdk/sign', 'JsApiController@sign'); # JSSDK签名

        # 微信红包
        $app->get('redpack', 'RedPackController@index'); # 红包发送记录
        $app->get('redpack/single', 'RedPackController@single'); # 发送普通红包
        $app->get('redpack/multiple', 'RedPackController@multiple'); # 发送裂变红包

    }
);

/**
 * 微信活动接口
 */
$app->group(
    [
        'prefix' => 'wechat/mp',
        'namespace' => 'Wechat'
    ],
    function () use ($app) {

        $app->post('game/list', 'MpGameController@getMpGameList');  # 获取游戏详细信息
        $app->post('game/info', 'MpGameController@getMpGameInfo');  # 获取游戏详细信息
        $app->post('game/play', 'MpGameController@playGame');  # 开始玩儿游戏
        $app->post('game/win', 'MpGameController@getGameReward');  # 获取游戏分值
        $app->post('member/info', 'MpGameController@getMemberInfo');  # 拉取玩家信息
        $app->post('luck/draw', 'MpGameController@getLuckDraw');  # 用户抽奖

        $app->post('my/project', 'MpController@getMyProject');  # 获取我的项目列表
        $app->post('game/source/list', 'MpController@getGameSourceList');  # 获取项目游戏素材
        $app->post('game/source/image/upload', 'MpController@getGameSourceImageUpload');  # 上传项目图片素材
        $app->post('game/source/music/upload', 'MpController@getGameSourceMusicUpload');  # 上传背景音乐素材
        $app->post('project/set', 'MpController@setProjectInfo');  # 设置活动信息

        $app->post('game/pay', 'PayGameController@payGame');  # 购买游戏
        $app->post('money/recharge', 'PayGameController@recharge');  # 用户充值
        $app->post('redpage/grant', 'PayGameController@grantRedpage');  # 发放红包

        $app->post('game/statistics', 'GameDataController@statistics');  # 用户统计
        $app->post('game/redpage/envelope', 'GameDataController@redpageEnvelope');  # 口令红包
        $app->post('game/ranking', 'GameDataController@ranking');  # 排行榜

    }
);