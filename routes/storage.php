<?php

/**
 * 阿里云OSS
 */
$app->group(
    [
        'prefix' => 'storage/oss',
        'namespace' => 'Storage'
    ],
    function () use ($app) {
        $app->post('upload/sign', 'OSSUploadController@sign'); #  OSS直传文件签名
        $app->post('upload/callback', 'OSSUploadController@callback'); #  OSS直传文件回调

        //$app->post('down', 'OSSController@download'); # 下载文件
        //$app->post('delete', 'OSSController@delete'); # 删除文件
        //$app->get('bucket', 'OSSBucketController@index'); # 查看全部存储空间
    }
);

/**
 * 七牛云存储
 */
$app->group(
    [
        'prefix' => 'storage/qiniu',
        'namespace' => 'Storage',
    ],
    function () use ($app) {

    }
);