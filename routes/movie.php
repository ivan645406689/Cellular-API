<?php

/**
 * 影视接口
 */
$app->group(
    [
        'prefix' => 'movie',
        'namespace' => 'Movie'
    ],
    function () use ($app) {

        $app->get('category', 'MovieController@category'); # 影视分类
        $app->get('tag', 'MovieController@tag'); # 标签列表
        $app->post('index', 'MovieController@index'); # 影视列表
        $app->post('search', 'MovieController@search'); # 搜索影视列表
        $app->post('show', 'MovieController@show'); # 影视详细信息
        $app->post('resource/index', 'MovieResourceController@index'); # 影视资源文件
        $app->post('resource/buy', 'MovieResourceController@buy'); # 影视资源购买
        $app->post('comment/index', 'MovieCommentController@index'); # 影视评论列表
        $app->post('comment/store', 'MovieCommentController@store'); # 影视评论发布
        $app->post('recommend', 'MovieController@recommend'); # 影视推荐列表

    }
);

/**
 * 会员和点券自动充值接口
 */
$app->group(
    [
        'prefix' => 'movie/callback',
        'namespace' => 'Movie'
    ],
    function () use ($app) {

        $app->post('coupon', 'MovieCallBackController@coupon'); # 观影券购买回调
        $app->post('vip', 'MovieCallBackController@vip'); # VIP购买回调

    }
);

