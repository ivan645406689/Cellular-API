<?php

/**
 * 支付宝接口
 */
$app->group(
    [
        'prefix' => 'pay/ali',
        'namespace' => 'Pay'
    ],
    function () use ($app) {

    }
);

/**
 * 微信支付接口
 */
$app->group(
    [
        'prefix' => 'pay/wechat',
        'namespace' => 'Pay'
    ],
    function () use ($app) {

    }
);

/**
 * 银联支付接口
 */
$app->group(
    [
        'prefix' => 'pay/union',
        'namespace' => 'Pay'
    ],
    function () use ($app) {

    }
);

/**
 * 苹果闪付接口
 */
$app->group(
    [
        'prefix' => 'pay/apple',
        'namespace' => 'Pay'
    ],
    function () use ($app) {

    }
);

/**
 * 苹果内购iap接口
 */
$app->group(
    [
        'prefix' => 'pay/iap',
        'namespace' => 'Pay'
    ],
    function () use ($app) {

    }
);