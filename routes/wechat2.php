<?php

/**
 * 微信公众号接口
 */
$app->group(
    [
        'prefix' => 'wechat',
        'namespace' => 'Wechat',
        'middleware' => 'wechat'
    ],
    function () use ($app) {

        //$app->get('receive', 'ReceiveController@check');  # 验证微信第三方服务器地址接口
        $app->post('receive', 'ReceiveController@index');  # 接收推送处理接口

        # 菜单管理
        $app->get('menu/index', 'MenuController@index'); # 获取自定义菜单
        $app->get('menu/store', 'MenuController@store'); # 创建自定义菜单

        # 媒体素材管理
        $app->get('media/count', 'MediaController@count'); # 媒体素材总数
        $app->get('media/index', 'MediaController@index'); # 媒体素材列表

        # oauth 身份验证
        $app->get('oauth/code', 'OauthController@code'); # 获取 snsapi 授权链接
        $app->get('oauth/user', 'OauthController@user'); # code回调，拉取用户信息

        # 生成二维码
        $app->get('qr', 'QRCodeController@scene'); # 生成带参数临时二维码

        # 分享接口
        $app->post('jssdk/sign', 'JsApiController@sign'); # JSSDK签名

        # 微信红包
        $app->get('redpack', 'RedPackController@index'); # 红包发送记录
        $app->get('redpack/single', 'RedPackController@single'); # 发送普通红包
        $app->get('redpack/multiple', 'RedPackController@multiple'); # 发送裂变红包

    }
);

/**
 * 微信活动接口
 */
$app->group(
    [
        'prefix' => 'wechat/activity',
        'namespace' => 'Wechat'
    ],
    function () use ($app) {
        
        $app->post('game/info', 'ActivityController@getGameInfo');  # 获取游戏信息
        $app->post('game/wechatinfo', 'ActivityController@getWechatMpGameInfo');  # 获取公众号相关游戏信息
        $app->post('game/reward', 'ActivityController@getGameReward');  # 获取游戏奖励
        $app->post('game/win', 'GameController@winGame');  # 赢取游戏

    }
);