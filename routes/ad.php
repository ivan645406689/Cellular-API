<?php
/**
 * Created by PhpStorm.
 * User: Administrator
 * Date: 2016/12/1
 * Time: 15:11
 */
$app->group(
    [
        'prefix' => 'ad',
        'namespace' => 'AD'
    ],
    function () use ($app) {

        $app->get('index', 'ADController@index'); # 广告列表

    }
);