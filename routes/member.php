<?php

/**
 * 会员用户信息接口
 */
$app->group(
    [
        'prefix' => 'member',
        'namespace' => 'Member',
    ],
    function () use ($app) {
        $app->post('/', 'MemberController@index'); # 查看全部会员列表
        $app->post('show', 'MemberController@show'); # 查看公开信息
        $app->post('edit', 'MemberController@edit'); # 个人详细信息
        $app->post('update', 'MemberController@update'); # 修改详细信息
        $app->post('password', 'MemberController@password'); # 修改登录密码
    }
);

/**
 * 会员身份验证接口
 */
$app->group(
    [
        'prefix' => 'member/guest',
        'namespace' => 'Member'
    ],
    function () use ($app) {
        $app->post('login', 'GuestController@login'); # 登录
        $app->post('register', 'GuestController@register'); # 注册新用户
        $app->post('register/code/phone', 'GuestController@registerPhone'); # 发送注册短信验证码
        $app->post('register/code/email', 'GuestController@registerEmail'); # 发送注册邮箱验证邮件
        $app->post('forgot', 'GuestController@forgot'); # 短信验证码找回密码
        $app->post('forgot/code/phone', 'GuestController@forgotPhone'); # 发送找回密码短信验证码
        $app->post('forgot/code/email', 'GuestController@forgotEmail'); # 发送找回密码验证邮件
    }
);

/**
 * 会员钱包接口
 */
$app->group(
    [
        'prefix' => 'member/money',
        'namespace' => 'Member',
    ],
    function () use ($app) {
        $app->post('/', 'MemberMoneyController@index'); # 查看余额变动记录
        $app->post('show', 'MemberMoneyController@show'); # 查看余额记录详细
        $app->post('pay', 'MemberMoneyController@pay'); # 余额充值
    }
);