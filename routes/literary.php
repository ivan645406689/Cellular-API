<?php

/**
 * 小说文学接口
 */
$app->group(
    [
        'prefix' => 'literary',
        'namespace' => 'Literary'
    ],
    function () use ($app) {

        $app->get('test', 'LiteraryController@test');

    }
);