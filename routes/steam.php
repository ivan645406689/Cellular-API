<?php
/**
 * 游戏
 */
$app->group(
    [
        'prefix' => 'steam/game',
        'namespace' => 'Steam'
    ],
    function () use ($app) {

        $app->post('recommend', 'GameController@recommend'); # 精选和推荐
        $app->post('preferential', 'GameController@preferential');# 特别优惠
        $app->post('details', 'GameController@details');# 游戏详情
        $app->post('picture', 'GameController@picture');# 游戏图片列表
        $app->post('video', 'GameController@video');# 游戏视频列表
        $app->post('goods', 'GameController@goods');# 游戏商品列表（商品版本,捆绑包,DLC）
        $app->post('news', 'GameController@news');# 最近更新 （新闻）
        $app->post('tags', 'GameController@tags');# 该游戏所属标签列表
        $app->post('addTag', 'GameController@addTag');# 添加标签
        $app->post('collocation', 'GameController@collocation');# 游戏配置要求
        $app->post('critique ', 'GameController@critique ');# 鉴赏家评论列表
        $app->post('similar ', 'GameController@similar ');# 更多类似产品
        $app->post('evaluating ', 'GameController@evaluating ');# 消费者 评测列表

    }
);
/**
 * 软件
 */
$app->group(
    [
        'prefix' => 'steam/soft',
        'namespace' => 'Steam'
    ],
    function () use ($app) {

        $app->post('recommend', 'SoftController@recommend'); # 精选和推荐
        $app->post('preferential', 'SoftController@preferential');# 特别优惠
        $app->post('details', 'SoftController@details');# 软件详情
        $app->post('picture', 'SoftController@picture');# 软件图片列表
        $app->post('video', 'SoftController@video');# 软件视频列表
        $app->post('goods', 'SoftController@goods');# 软件商品列表（商品版本,捆绑包,DLC）
        $app->post('news', 'SoftController@news');# 最近更新 （新闻）
        $app->post('tags', 'SoftController@tags');# 该软件所属标签列表
        $app->post('addTag', 'SoftController@addTag');# 添加标签
        $app->post('collocation', 'SoftController@collocation');# 配置要求
        $app->post('critique ', 'SoftController@critique ');# 鉴赏家评论列表
        $app->post('similar ', 'SoftController@similar ');# 更多类似产品
        $app->post('evaluating ', 'SoftController@evaluating ');# 消费者 评测列表

    }
);
/**
 * 视频
 */
$app->group(
    [
        'prefix' => 'steam/video',
        'namespace' => 'Steam'
    ],
    function () use ($app) {

        $app->post('recommend', 'VideoController@recommend'); # 精选和推荐
        $app->post('preferential', 'VideoController@preferential');# 特别优惠
        $app->post('details', 'VideoController@details');# 视频详情
        $app->post('picture', 'VideoController@picture');# 视频图片列表
        $app->post('video', 'VideoController@video');# 视频列表
        $app->post('goods', 'VideoController@goods');# 商品列表（商品版本,捆绑包,DLC）
        $app->post('news', 'VideoController@news');# 最近更新 （新闻）
        $app->post('tags', 'VideoController@tags');# 该视频所属标签列表
        $app->post('addTag', 'VideoController@addTag');# 添加标签
        $app->post('collocation', 'VideoController@collocation');# 配置要求
        $app->post('critique ', 'VideoController@critique ');# 鉴赏家评论列表
        $app->post('similar ', 'VideoController@similar ');# 更多类似产品
        $app->post('evaluating ', 'VideoController@evaluating ');# 消费者 评测列表

    }
);