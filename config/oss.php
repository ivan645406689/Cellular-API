<?php

return [

    # AccessKeyId
    'access_key_id' => 'd4W8AMhjnWmnqsBY',

    # AccessKeySecret
    'access_key_secret' => 'yVWFy7nJeNylKPXxMosbf5HiCVT3xv',

    # OSS区域地址
    'endpoint' => 'oss-cn-beijing.aliyuncs.com',

    # 存储空间名称
    'bucket' => 'video-dance',

    # 请求超时时间，单位秒，默认是5184000秒, 这里建议 不要设置太小，如果上传文件很大，消耗的时间会比较长
    'time_out' => 3600,

    # 连接超时时间，单位秒，默认是10秒
    'connect_time_out' => 10

];