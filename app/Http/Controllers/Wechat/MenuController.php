<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ext\Wechat\Menu;

class MenuController extends Controller
{

    public function index(Request $request)
    {
        $data = Menu::current($request->wechat->token);
        return $this->success(['data' => $data]);
    }

    public function store(Request $request)
    {
        $param = [
            'button' => [
                [
                    'type' => 'view',
                    'name' => '每日爆料',
                    'url' => 'http://baidu.com'
                ],
                [
                    'type' => 'view',
                    'name' => '影片资源',
                    'url' => 'http://163.com'
                ],
                [
                    'type' => 'view',
                    'name' => '会员中心',
                    'url' => 'http://qq.com'
                ],
            ]
        ];
        $callback = Menu::create($request->wechat->token, $param);
        return $this->success(['status' => $callback]);
    }
}
