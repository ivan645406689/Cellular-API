<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ext\Wechat\User;
use App\Ext\Wechat\Oauth;
use App\Model\Wechat;

class OauthController extends Controller
{

    /**
     * 获取 snsapi_base 授权链接
     *
     * @param Request $request
     * @param string $scope # 应用授权作用域 (base: 静默授权; user: 弹出授权页面，未关注也可获取)
     */
    public function code(Request $request)
    {
        $scope = $request->has('user') ? 'snsapi_userinfo' : 'snsapi_base';
        $redirect_uri = url('wechat/oauth/user');
        $url = Oauth::code($request->wechat->config['app_id'], $redirect_uri, $scope, $request->wechat->config['id']);
        header('Location: ' . $url);
    }

    /**
     * Oauth 授权获取用户信息回调
     *
     * @param Request $request
     * @return array
     */
    public function user(Request $request)
    {
        $code = $request->input('code');
        if ($code) {
            $result = Oauth::authToken($request->wechat->config['app_id'], $request->wechat->config['app_secret'], $code);
            if ($result != false) {
                $response = [];
                $response['state'] = $request->input('state');
                $response['openid'] = $result->openid;
                if ($result->scope == 'snsapi_userinfo') {
                    # 通过手动授权获取用户信息
                    $user = Oauth::userInfo($result->access_token, $result->openid);
                    # 写入数据库
                    $wechat = Wechat::where('openid', $user->openid)->first();
                    if (!$wechat) $wechat = new Wechat();
                    $wechat->openid = $user->openid;
                    $wechat->nickname = $user->nickname;
                    $wechat->language = $user->language;
                    $wechat->sex = $user->sex;
                    $wechat->city = $user->city;
                    $wechat->province = $user->province;
                    $wechat->picture = $user->headimgurl;
                    $wechat->wechat_mp_id = $request->wechat->config['id'];
                    $wechat->save();
                    $response['id'] = $wechat->id;
                } else {
                    # 静默获取用户信息，如果未关注不能获取信息
                    $user = User::unionID($request->wechat->token, $result->openid);
                    # 写入数据库
                    $wechat = Wechat::where('openid', $user->openid)->first();
                    if (!$wechat) $wechat = new Wechat();
                    $wechat->openid = $user->openid;
                    $wechat->nickname = $user->nickname;
                    $wechat->language = $user->language;
                    $wechat->sex = $user->sex;
                    $wechat->city = $user->city;
                    $wechat->province = $user->province;
                    $wechat->country = $user->country;
                    $wechat->picture = $user->headimgurl;
                    $wechat->subscribe = $user->subscribe;
                    $wechat->subscribe_time = $user->subscribe_time;
                    $wechat->wechat_mp_id = $request->wechat->config['id'];
                    $wechat->save();
                    $response['id'] = $wechat->id;
                }
                return $this->success(['data' => $response]);
            }
            return $this->error('2', 'result error');
        }
        return $this->error('1', 'code error');
    }

}
