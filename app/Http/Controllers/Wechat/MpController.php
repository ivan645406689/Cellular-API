<?php

namespace App\Http\Controllers\Wechat;

use App\DB\WechatMpGameDB;
use App\Http\Controllers\Controller;
use App\Model\Common;
use App\Model\WechatMpGame;
use Illuminate\Http\Request;

class MpController extends Controller
{
    /**
     * 获取公众号游戏列表
     *
     * @param Request $request
     * @return array
     */
    public function getMyProject(Request $request)
    {
        # 公众号游戏ID
        if(!$request->has('id')) {
            return $this->error(1, 'No found id!');
        }
        $id = $request->input('id');
        $page = ($request->has('page')) ? $request->input('page') : 1;
        $limit = ($request->has('limit')) ? $request->input('limit') : 10;
        # 查询微信公众号小游戏活动表信息
        $response = WechatMpGameDB::getMpGameList($id, $page, $limit);
        if(!$response) {
            return $this->error(1, 'Data error!');
        }
        return $this->success($response);
    }
}
