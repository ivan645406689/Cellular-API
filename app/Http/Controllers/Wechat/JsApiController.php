<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ext\Wechat\Common;

class JsApiController extends Controller
{

    private function ticket(Request $request)
    {
        if ($this->redisCache()->check('wechat:ticket') == 1) {
            return $this->redisCache()->get('wechat:ticket');
        } else {
            $result = Common::ticket($request->wechat->token);
            if ($result) {
                $this->redisCache()->set('wechat:ticket', $result->expires_in - 300, $result->ticket);
                return $result->ticket;
            }
        }
        return false;
    }

    public function sign(Request $request)
    {
        $url = $request->input('url');
        if ($url) {
            $param = [];
            $param['app_id'] = $request->wechat->config['app_id'];
            $ticket = $this->ticket();
            $param += Common::ticketSign($ticket, $url);
            return $param;
        }
        return $this->error('1', '签名URL参数不正确!');
    }

}
