<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ext\Wechat\RedPack;
use App\Model\WechatRedpack;
use App\Model\WechatActivity;

class RedPackController extends Controller
{
    /**
     * 查询红包订单
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        if (!$request->has('id')) {
            return $this->error('1', '请求参数不能为空!');
        }
        $id = $request->input('id');
        $redPack = WechatRedpack::find($id);
        if (!empty($redPack->mch_billno)) {
            $cert = $this->cert();
            $result = RedPack::search($redPack->mch_billno, $request->wechat->config['mch_id'], $request->wechat->config['app_id'], $request->wechat->config['api_key'], $cert);
            if ($result['error'] == 0) {
                return $this->success($result);
            }
            return $this->error('1', [
                'err_code' => $result['code'],
                'err_des' => $result['msg']
            ]);
        }
        return $this->error(1, '没有查询结果！');
    }

    /**
     * 发送普通红包
     *
     * @param Request $request
     * @return array
     */
    public function single(Request $request)
    {
        if (!$request->has('id')) {
            return $this->error('1', '请求参数不能为空!');
        }
        $id = $request->input('id');
        $redPack = WechatRedpack::find($id);
        if ($redPack && $redPack->status == 0 && empty($redPack->mch_billno)) {
            $activity = WechatActivity::find($redPack->wechat_activity_id);
            if ($activity) {
                $price = bcmul($redPack->price, 100); # 红包金额，微信金额以分为单位
                $param = [
                    'mch_billno' => RedPack::billno($request->wechat->config['mch_id']),
                    'mch_id' => $request->wechat->config['mch_id'],
                    'app_id' => $request->wechat->config['app_id'],
                    'name' => $activity->mp->title,
                    'openid' => $redPack->openid,
                    'price' => $price,
                    'wishing' => $redPack->wishing,
                    'title' => $activity->title,
                    'remark' => $redPack->remark,
                    'client_ip' => getServerIP()
                ];
                $cert = $this->cert();
                $result = RedPack::send($request->wechat->config['api_key'], $param, $cert);
                if ($result['error'] == 1) {
                    # 保存红包发送信息
                    $redPack->mch_billno = $param['mch_billno'];
                    $redPack->status = 1;
                    $redPack->save();
                    return $this->success();
                }
                return $this->error('1', [
                    'err_code' => $result['code'],
                    'err_des' => $result['msg']
                ]);
            }
            return $this->error('1', '活动不存在！');
        }
        return $this->error('1', '请求参数不正确！');
    }

    /**
     * 裂变红包
     */
    public function multiple()
    {

    }
}
