<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Ext\Wechat\Media;

class MediaController extends Controller
{

    /**
     * 查询媒体素材列表
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $type = $request->input('type');
        $page = $request->input('page', 1);
        $limit = $request->input('limit', 20);
        $result = Media::get($request->wechat->token, $type, $page, $limit);
        return $this->success(['data' => $result]);
    }

    /**
     * 查询媒体素材总数
     *
     * @return mixed
     */
    public function count(Request $request)
    {
        return Media::count($request->wechat->token);
    }

}
