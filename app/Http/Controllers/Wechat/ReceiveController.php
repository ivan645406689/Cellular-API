<?php

namespace App\Http\Controllers\Wechat;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Log;
use App\Ext\Wechat\Push;
use App\Ext\Wechat\User;
use App\Model\Wechat;
use App\Model\WechatReply;

class ReceiveController extends Controller
{

    public function check(Request $request)
    {
        $push = new Push();
        if ($push->valid($request->wechat->config['token'])) {
            return $request->input('echostr');
        }
    }

    public function index(Request $request)
    {
        $push = new Push();
        if ($push->valid($request->wechat->config['token'])) {
            $type = $push->receive()->getType();
            switch ($type) {
                # 文字消息
                case $push::MSG_TEXT:
                    # 接收微信服务器推送消息
                    $receiveMsg = $push->receive()->getContent();
                    # 查询数据库对应的返回消息
                    $replyMsg = WechatReply::getReply($receiveMsg);
                    # 返回微信服务器响应消息
                    if ($replyMsg) {
                        $push->text($replyMsg)->reply();
                    }
                    break;
                # 事件消息
                case $push::MSG_EVENT:
                    $event = $push->getEvent();
                    $openID = $push->getFrom();
                    $scene = isset($event['key']) ? $event['key'] : null;
                    switch ($event['event']) {
                        # 关注事件
                        case $push::EVENT_SUBSCRIBE:
                            $this->subscribe($openID, $scene);
                            $push->text('感谢关注')->reply();
                            break;
                        # 取消关注事件
                        case $push::EVENT_UNSUBSCRIBE:
                            $this->unSubscribe($openID);
                            break;
                        # 已关注用户描关带场景值二维码事件
                        case $push::EVENT_SCAN:
                            $this->subscribe($openID, $scene);
                            break;
                        # 上报地理位置
                        case $push::EVENT_LOCATION:
                            // $push->text()->reply(); # 上报地理位置信息
                            break;
                    }
                    break;
                default:
                    $push->text('hello!')->reply();
            }
        }
    }

    private function subscribe(Request $request, $openID, $scene = null)
    {
        # 查询是否存在用户信息
        $wechat = Wechat::where('openid', $openID)->first();
        if (!$wechat) {
            $wechat = new Wechat();
            $wechat->openid = $openID;
            # 通过openid拉取用户信息
            $user = User::unionID($request->wechat->token, $openID);
            if ($user) {
                $wechat->nickname = $user->nickname;
                $wechat->language = $user->language;
                $wechat->sex = $user->sex;
                $wechat->city = $user->city;
                $wechat->province = $user->province;
                $wechat->country = $user->country;
                $wechat->picture = $user->headimgurl;
            }
        }
        $wechat->subscribe = '1';
        $wechat->subscribe_time = date('Y-m-d H:i:s');
        $wechat->wechat_mp_id = $request->wechat->config['id'];
        if (!empty($scene)) {
            $wechat->qrscene = str_replace('qrscene_', '', $scene);
        }
        $wechat->save();
    }

    private function unSubscribe($openID)
    {
        $wechat = Wechat::where('openid', $openID)->first();
        $wechat->subscribe = '0';
        $wechat->save();
    }
}
