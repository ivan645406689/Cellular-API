<?php

namespace App\Http\Controllers\Mall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\ProductCategory;
class CategoryController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }
    /**
     *
     * 商品分类接口
     * 请求：
     * parent_id:层级id（可以不要）
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function index(Request $request)
    {
        $parent_id = $request->has('parent_id') ? $request->input('parent_id') : 0;
        if (!is_numeric($parent_id)) {
            return $this->error(1,'参数错误');
        }
        $categoryList = ProductCategory::getProductCategory($parent_id);
        $response = [];
        $response['data'] = $categoryList;
        return $this->success($response);
    }

}