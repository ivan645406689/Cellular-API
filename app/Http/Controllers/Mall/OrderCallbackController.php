<?php

namespace App\Http\Controllers\Mall;

use App\DB\MovieVipDB;
use App\Http\Controllers\Controller;
use App\Model\MovieProduct;
use Illuminate\Http\Request;
use App\Model\Order;
class OrderCallbackController extends Controller
{

    /**
     *
     * 虚拟订单回调接口
     *
     * @param Request $request
     */
    public function virtualCallback(Request $request, $mp)
    {
        if(!$request->has('order_id')) {
            return $this->error(1, 'No found order_id');
        }
        $orderId = $request->input('order_id');
        $order = Order::find($orderId);
        if(!$order) {
            return $this->error(2, 'No found order');
        }
        # 订单状态，如果是已完成，则无法进行交易
        if($order->status > 0) {
            return $this->error(3, 'Deal is closed');
        }
        # 如果回调字段无数据，证明不是虚拟订单，则不可以进行自动发货操作
        if($order->virtual_callback == null) {
            return $this->error(4, 'Is not virtual');
        }
        # 获得影视商品
        $movieProduct = MovieProduct::find($mp);
        if(!$movieProduct) {
            return $this->error(5, 'No found movie product');
        }
        $data = [
          'order_id' => $orderId,
          'type' => $movieProduct->type,
          'amount' => $movieProduct->amount,
          'member_id' => $order->member_id
        ];
        # 进行确认订单及虚拟商品自动发货
        $result = MovieVipDB::autoDelivery($data);
        if(!$result) {
            return $this->error(6, 'Deal is fail');
        }
        $response = [];
        $response['member_id'] = $order->member_id;
        return $this->success($response);
    }
}