<?php

namespace App\Http\Controllers\Mall;

use App\DB\OrderDB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class OrderMemberController extends Controller
{
    /**
     *
     * 会员订单列表
     * 请求：
     * member_id:会员ID
     * $page:页数
     * $limit：每页显示条数
     *
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function index(Request $request)
    {
        # 会员ID
        $member_id = $request->input('member_id');
        if (!is_numeric($member_id)) {
            return $this->error(1,'参数错误');
        }
        $page = $request->has('page') ? $request->input('page') : 1; # 当前页数
        if (!is_numeric($page)) {
            return $this->error(2,'参数错误');
        }
        $limit = $request->has('limit') ? $request->input('limit') : 10; # 每页显示条数
        if (!is_numeric($limit)) {
            return $this->error(3,'参数错误');
        }
        $orderList = OrderDB::getMemberOrderList($member_id,$page,$limit);
        $response = [];
        $response['data'] = $orderList;
        return $this->success($response);
    }
    /**
     *
     * 订单信息
     * 请求：
     * order_id:订单ID
     *
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function show(Request $request)
    {
        # 订单ID
        $orderId = $request->input('id');
        if (!is_numeric($orderId)) {
            return $this->error(1,'参数错误');
        }
        $order = OrderDB::getOrder($orderId);
        $response = [];
        $response['data'] = $order;
        return $this->success($response);
    }
}

