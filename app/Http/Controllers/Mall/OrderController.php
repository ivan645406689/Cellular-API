<?php

namespace App\Http\Controllers\Mall;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Model\Order;
use App\Model\Member;
use App\Model\Product;
class OrderController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }
    /**
     *
     * 商品购买下单
     * 请求：
     * member_id:会员id
     * product_id:商品ID
     * total：订单数量
     * is_virtual:是否虚拟商品
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function store(Request $request)
    {
        # 会员id
        $memberId = $request->input('member_id');
        if (!is_numeric($memberId)) {
            return $this->error(1, '参数错误');
        }
        # 验证用户是否存在
        $member = Member::find($memberId);
        if(!$member){
            return $this->error(2, '用户不存在');
        }
        # 商品id
        $productId = $request->input('product_id');
        if (!is_numeric($productId)) {
            return $this->error(3, '参数错误');
        }
        # 验证商品是否存在
        $product = Product::where('is_delete', 0)->find($productId);
        if(!$product){
            return $this->error(4, '商品不存在');
        }
        if($product->status == 0) {
            return $this->error(5, '商品已下架');
        }
        # 订单数量
        $total = $request->input('total') ? $request->input('total') : 1;
        if (!is_numeric($total)) {
            return $this->error(6, '参数错误');
        }
        if($product->amount<$total){
            return $this->error(7, '商品库存数量不足');
        }
        # 商品总价
        $price = bcmul($product->sale_price, $total, 2);

//        # 是否余额支付
//        $remaining = $request->input('remaining') ? $request->input('remaining') : 0;
//        if($remaining){
//            if($member->money<$price){
//                return $this->error('用户余额不足');
//            }
//        }

        # 是否是虚拟商品
        $is_virtual = $request->has('is_virtual') ? $request->input('is_virtual') : 1;
        if (!is_numeric($is_virtual)) {
            return $this->error(8, '参数错误');
        }
        # 添加订单信息
        $info = [
            'member_id' => $memberId,
            'product_id' => $productId,
            'total' => $total,
            'price' => $price,
            'is_virtual' =>$is_virtual,
//            'remaining' =>$remaining,
            'virtual_callback' =>$product ->virtual_callback
        ];
        
        $result = Order::createOrder($info);
        if(!$result){
            return $this->error(8, '下单失败');
        }
        $response = [];
        $response['data'] = $result;
        return $this->success($response);
    }

}