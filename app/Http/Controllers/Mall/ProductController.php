<?php

namespace App\Http\Controllers\Mall;

use App\DB\ProductDB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
class ProductController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }
    /**
     *
     * 商品列表
     * 请求：
     * status:商品状态
     * is_virtual：虚拟商品
     * page:当前页数
     * limit:每页显示条数
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function index(Request $request)
    {
        $status = $request->has('status') ? $request->input('status') : 1;# 商品状态1上架0下架
        if (!is_numeric($status)) {
            return $this->error(1,'参数错误');
        }
        $is_virtual = $request->has('is_virtual') ? $request->input('is_virtual') : 1; #是否虚拟商品
        if (!is_numeric($is_virtual)) {
            return $this->error(2,'参数错误');
        }
        $page = $request->has('page') ? $request->input('page') : 1; # 当前页数
        if (!is_numeric($page)) {
            return $this->error(3,'参数错误');
        }
        $limit = $request->has('limit') ? $request->input('limit') : 10; # 每页显示条数
        if (!is_numeric($limit)) {
            return $this->error(4,'参数错误');
        }
        $productList = ProductDB::getProductList($status,$is_virtual,$page,$limit);
        $response = [];
        $response['data'] = $productList;
        return $this->success($response);
    }
    /**
     *
     * 商品详细信息
     * 请求：
     * product_id:商品id
     *
     * example
     *
     * 返回：
     * error: 错误码
     * msg: 错误信息说明
     * data:[
     * ]
     * @param
     * @return array json
     */
    public function show(Request $request)
    {
        $product_id = $request->input('id');
        if (!is_numeric($product_id)) {
            return $this->error(1,'参数错误');
        }
        $product = ProductDB::getProduct($product_id);

        $response = [];
        $response['data'] = $product;
        return $this->success($response);
    }
}