<?php
namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

/**
 * Class UserController
 * @package App\Http\Controllers\Member
 */
class MemberMoneyController extends Controller
{
    /**
     * 查看余额变动记录
     * @param Request $request
     * @return array
     */
    public function index(Request $request) {
        $userID = $request->input('user_id');
        $sign = $request->input('sign');
        $data = $request->except('sign');
        if (!$this->checkSign($userID, $data, $sign)) {
            return $this->error(1);
        }
        $page = $request->input('page'); # 当前页数
        $limit = $request->input('limit'); # 每页显示条数
        if (!$limit) $limit = 10; # 默认每页显示10条
        $param = [
            'data' => [],
            'total' => 0,
            'total_page' => 0
        ];
        return $this->success($param);
    }

    /**
     * 查看余额记录详细
     * @param Request $request
     * @return array
     */
    public function show(Request $request) {
        $userID = $request->input('user_id');
        $sign = $request->input('sign');
        $data = $request->except('sign');
        if (!$this->checkSign($userID, $data, $sign)) {
            return $this->error(1);
        }
        $id = $request->input('id');
        $param = [];
        return $this->success($param);
    }

    /**
     * 余额充值
     * 功能待定
     * @param Request $request
     * @return array
     */
    public function pay(Request $request) {
        $userID = $request->input('user_id');
        $sign = $request->input('sign');
        $data = $request->except('sign');
        if (!$this->checkSign($userID, $data, $sign)) {
            return $this->error(1);
        }
        return $this->success();
    }
}