<?php
namespace App\Http\Controllers\Member;

use App\Http\Controllers\Controller;
use App\Model\Member;
use Illuminate\Http\Request;

/**
 * Class MemberController
 * @package App\Http\Controllers\Member
 */
class MemberController extends Controller
{
    /**
     * 查看全部会员列表
     * 返回全部用户的基本信息列表
     * @param Request $request
     * @return array
     */
    public function index(Request $request) {
        $page = ($request->has('page')) ? $request->input('page') : 1; # 当前页数
        $limit = ($request->has('limit')) ? $request->input('limit') : 10;# 每页显示条数
        $responses = Member::getMemberList($page, $limit);
        if(!$responses) {
            return $this->error(1, 'No found data');
        }
        return $this->success($responses);
    }

    /**
     * 查看会员公开信息
     * @param Request $request
     * @return array
     */
    public function show(Request $request) {
        if(!$request->has('member_id')) {
            return $this->error(1, 'No found member_id');
        }
        $memberID = $request->input('member_id');
        $responses = Member::getShowMember($memberID);
        if(!$responses) {
            return $this->error(2, 'No found data');
        }
        $responses['avatar_path'] = 'upload/image/user/avatar/';
        return $this->success($responses);
    }

    /**
     * 查看会员个人详细信息
     * @param Request $request
     * @return array
     */
    public function edit(Request $request) {
        $memberID = $request->input('member_id');
//        $sign = $request->input('sign');
//        $data = $request->except('sign');
//        if (!$this->checkSign($memberID, $data, $sign)) {
//            return $this->error(1, 'No sign');
//        }
        $responses = Member::getMemberDetail($memberID);
        if(!$responses) {
            return $this->error(2, 'No found data');
        }
        $responses['avatar_path'] = 'upload/image/user/avatar/';
        return $this->success($responses);
    }

    /**
     * 修改会员详细信息
     * @param Request $request
     * @return array
     */
    public function update(Request $request) {
        $memberID = $request->input('member_id');
//        $sign = $request->input('sign');
//        $data = $request->except('sign');
//        if (!$this->checkSign($memberID, $data, $sign)) {
//            return $this->error(1, 'No sign');
//        }
        if(!$request->has('data')) {
            return $this->error(2, 'No found data');
        }
        $data = $request->input('data'); # 会员详细信息参数
        $data = json_decode($data);
        # 验证电子邮箱
        if(!isset($data->email)) {
            return $this->error(3, 'No found email');
        }
        $email = $data->email;
        if(!$this->regex('email', $email)) {
            return $this->error(4, 'Email is error');
        }
//        # 验证手机
//        if(!isset($data['mobile'])) {
//            return $this->error(5, 'No found mobile');
//        }
//        $mobile = $data['mobile'];
//        if(!$this->regex('mobile', $mobile)) {
//            return $this->error(6, 'Mobile is error');
//        }
        # 验证真实姓名
        if(!isset($data->name)) {
            return $this->error(7, 'No found name');
        }
        $name = $data->name;
//        if(!$this->regex('name', $name)) {
//            return $this->error(8, 'Name is error');
//        }
        # 验证性别
        if(!isset($data->sex)) {
            return $this->error(9, 'No found sex');
        }
        # 验证生日
        if(!isset($data->birthday)) {
            return $this->error(10, 'No found birthday');
        }
        $result = Member::updateMember($memberID, $data);
        if(!$result) {
            return $this->error(11, 'Save fail');
        }
        $responses = [];
        $responses['id'] = $memberID;
        return $this->success($responses);
    }

    /**
     * 修改会员登录密码
     * @param Request $request
     * @return array
     */
    public function password(Request $request) {
        $memberID = $request->input('member_id');
//        $sign = $request->input('sign');
//        $data = $request->except('sign');
//        if (!$this->checkSign($memberID, $data, $sign)) {
//            return $this->error(1, 'No sign');
//        }
        if(!$request->has('original')) {
            return $this->error(2, 'No found original');
        }
        $original = $request->input('original');
        if(!$request->has('password')) {
            return $this->error(3, 'No found password');
        }
        $password = $request->input('password');
        if(!$this->regex('password', $password)) {
            return $this->error(4, 'Password is error');
        }
        if(!$request->has('agin_password')) {
            return $this->error(5, 'No found agin_password');
        }        
        $aginPassword = $request->input('agin_password');
        if($password != $aginPassword) {
            return $this->error(6, 'Password no same');
        }
        $result = Member::updatePassword($memberID, $password, $original);
        if(!$result) {
            return $this->error(7, 'Update password fail');
        }
        $responses = [];
        $responses['id'] = $memberID;
        return $this->success($responses);
    }
}