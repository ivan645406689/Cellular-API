<?php

namespace App\Http\Controllers;

use Laravel\Lumen\Routing\Controller as BaseController;
use App\Model\Regex;
use App\Ext\RedisCache;

class Controller extends BaseController
{
    /**
     * 验证访问者身份
     * 检查缓存的用户token与请求的token是否一致
     * @param $userID 用户ID
     * @param $token 用户身份令牌
     * @return bool
     */
    protected function auth($userID, $token)
    {
        $cacheToken = $this->redisCache()->getHash('user:'.$userID, 'token');
        if ($cacheToken && $cacheToken == $token) return true;
        return false;
    }

    /**
     * 检查数据签名
     * @param $userID
     * @param $data
     * @param $sign
     * @return bool
     */
    protected function checkSign($userID, $data, $sign)
    {
        # 读取用户信息
        $user = $this->redisCache()->getHash('user:'.$userID, 'token');
        if (empty($user)) return false;
        # 根据键按升序排列
        $data = ksort($data);
        # 生成签名字符串，将数组拼接成字符串，用"&"进行连接
        $str = implode('&', $data);
        # 检查数据签名是否正确
        if (sha1($str) == $sign) return true;
        return false;
    }

    /**
     * 错误信息返回
     *
     * @param $code # 错误状态码
     * @param null $msg # 错误消息
     * @param array $param # 返回参数
     * @return array
     */
    protected function error($code, $msg = null, $param = [])
    {
        $response = [
            'error' => 1,
            'code' => $code,
            'msg' => $msg
        ];
        $response += $param;
        return json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 成功信息返回
     * @param array $param
     * @return array
     */
    protected function success($param = [])
    {
        $response = [
            'error' => 0,
            'msg' => 'success'
        ];
        $response += $param;
        return json_encode($response, JSON_UNESCAPED_UNICODE);
    }

    /**
     * 缓存方法
     *
     * @param int $db
     * @return RedisCache
     */
    protected function redisCache($db = 0)
    {
        return new RedisCache();
    }

    /**
     * 正则验证
     *
     * @param $key # 表达式名称
     * @param $value # 验证参数
     * @return bool
     */
    protected function regex($key, $value)
    {
        $regex = null;
        if ($this->redisCache()->check('regex')) {
            $result = $this->redisCache()->getHash('regex');
            if (isset($result[$key])) $regex = $result[$key];
        }
        if ($regex == null) {
            $result = Regex::keyName();
            $this->redisCache()->setHash('regex', $result);
            if (isset($result[$key])) $regex = $result[$key];
        }
        if ($regex != null && preg_match($regex, $value)) {
            return true;
        }
        return false;
    }

    /**
     * 分页返回查询结果集
     *
     * @param $result # 查询结果集
     * @param $page # 分页数
     * @param $limit # 显示记录数
     * @return mixed
     */
    protected function page($result, $page, $limit)
    {
        if(!is_numeric($page) || empty($page)) $page = 1;
        $start = ($page - 1) * $limit;
        return $result->skip($start)->take($limit)->get();
    }
}
