<?php

namespace App\Http\Controllers\AD;

use App\Http\Controllers\Controller;
use App\Model\AD;
use Illuminate\Http\Request;

class ADController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }


    /**
     *
     * 获取广告列表
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $adList = AD::where('category_id', 1)->get();
        if(!$adList) {
            return $this->error(1, 'No found data');
        }
        $respons = [];
        $respons['data'] = $adList;
        return $this->success($respons);
    }
}
