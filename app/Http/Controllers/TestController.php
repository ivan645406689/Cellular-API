<?php

namespace App\Http\Controllers;

use App\DB\MovieDB;
use App\Model\Movie;
use Illuminate\Http\Request;

class TestController extends Controller
{
    public function movie()
    {
        $result = MovieDB::RecommendMovie(4, 0, 10);
        $result = Movie::whereIn('id', $result)->get()->toArray();
        return $result;
    }

    public function access(Request $request)
    {
        return $this->success(['data'=>'ok']);
    }
}