<?php

namespace App\Http\Controllers\Movie;

use App\DB\MovieResourceDB;
use App\Http\Controllers\Controller;
use App\Model\MovieBuyLog;
use App\Model\MoviePlayLog;
use App\Model\MovieResource;
use App\Model\MovieVip;
use Illuminate\Http\Request;

class MovieResourceController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     *
     * 播放影视资源文件
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        if(!$request->has('id')) {
            return $this->error(1, 'No found id');
        }
        $id = $request->input('id'); # 影视id
        if(!$request->has('member_id')) {
            return $this->error(2, 'No found member_id');
        }
        $memberId = $request->input('member_id'); # 用户id
        # 资源是否可看
        $canSee = MovieResource::verCanSee($id, $memberId);
        $respons = [];
        if(!$canSee) {
            $respons['can'] = 0;
            return $this->success($respons);
        }
        $data = MovieResource::select('id', 'create_time', 'update_time', 'rank', 'movie_id', 'title', 'type', 'click', 'price', 'path', 'is_new', 'is_hot', 'status')->find($id);
        if(!$data) {
            $this->error(3, 'No found resource');
        }
        $play = MoviePlayLog::where('member_id', $memberId)->where('movie_resource_id', $id)->count();
        if($play == 0) {
            $playLog = MovieResource::playResource($id, $memberId);
            if(!$playLog) {
                $this->error(4, 'Play log save error!');
            }
        }
        $respons['can'] = 1;
        $respons['data'] = $data->toArray();
        return $this->success($respons);
    }

    /**
     *
     * 影视资源购买
     *
     * @param Request $request
     * @return array
     */
    public function buy(Request $request)
    {
        if(!$request->has('id')) {
            return $this->error(1, 'No found id');
        }
        $id = $request->input('id'); # 影视id
        if(!$request->has('member_id')) {
            return $this->error(2, 'No found member_id');
        }
        $memberId = $request->input('member_id'); # 用户id
        # 查看是否购买过该资源
        $hasBuy = MovieBuyLog::where('member_id', $memberId)->where('movie_resource_id', $id)->count();
        if($hasBuy) {
            return $this->error(3, 'Already bought this resource, don\'t repeat purchase!');
        }
        # 查看该资源需要多少金额
        $movieResource = MovieResource::find($id);
        # 查看余额是否充足
        $movieVip = MovieVip::where('member_id', $memberId)->first();
        if(!$movieVip) {
            return $this->error(4, 'You don\'t have a movie ticket');
        }
        if($movieVip->coupon < $movieResource->price) {
            return $this->error(5, 'Movie tickets left');
        }
        $result = MovieResourceDB::buyResource($id, $memberId, $movieResource->price);
        if(!$result) {
            $this->error(6, 'Buy fail!');
        }
        $respons = [];
        $respons['data'] = $id;
        return $this->success($respons);
    }
}
