<?php

namespace App\Http\Controllers\Movie;

use App\DB\MovieDB;
use App\Http\Controllers\Controller;
use App\Model\Movie;
use App\Model\MovieCategory;
use App\Model\MovieMapCategory;
use Illuminate\Http\Request;

class MovieController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     *
     * 分类列表
     *
     * @param Request $request
     * @return array
     */
    public function category(Request $request)
    {
        $category = MovieCategory::getMovieCategory();
        $respons = [];
        $respons['data'] = $category;
        return $this->success($respons);
    }

    /**
     *
     * 标签
     *
     * @param Request $request
     * @return array
     */
    public function tag(Request $request)
    {
        $respons = [];
        $respons['data'] = '标签';
        return $this->success($respons);
    }

    /**
     *
     * 影视列表
     * 请求：
     * category_id:分类id（可以不要）
     * page:页码（默认为1）
     * limit:显示数量（默认为10）
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        $category = $request->has('category_id') ? $request->input('category_id') : 0; # 分类列表
        $page = $request->has('page') ? $request->input('page') : 1; # 当前页数
        $limit = $request->has('limit') ? $request->input('limit') : 10; # 每页显示条数
        $data = MovieDB::getMovieList($category, $page, $limit);
        $response = [];
        $response['data'] = $data;
        return $this->success($response);
    }

    /**
     *
     * 搜索影视
     *
     * @param Request $request
     * @return array
     */
    public function search(Request $request)
    {
        $respons = [];
        $respons['data'] = '搜索影视';
        return $this->success($respons);
    }

    /**
     *
     * 影视详细信息
     *
     * @param Request $request
     * @return array
     */
    public function show(Request $request)
    {
        if(!$request->has('id')) {
            return $this->error(1, 'No found id');
        }
        $id = $request->input('id'); # 影视id
        $data = Movie::getMovie($id);
        $respons = [];
        $respons['data'] = $data;
        return $this->success($respons);
    }

    /**
     *
     * 影视推荐
     *
     * @param Request $request
     * @return array
     */
    public function recommend(Request $request)
    {
        if(!$request->has('id')) {
            return $this->error(1, 'No found id');
        }
        $id = $request->input('id'); # 影视id
        $data = Movie::getRecommendList($id);
        $respons = [];
        $respons['data'] = $data;
        return $this->success($respons);
    }
}
