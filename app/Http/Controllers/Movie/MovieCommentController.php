<?php

namespace App\Http\Controllers\Movie;

use App\DB\MovieCommentDB;
use App\Http\Controllers\Controller;
use App\Model\MovieComment;
use Illuminate\Http\Request;

class MovieCommentController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     *
     * 影视评论列表
     *
     * @param Request $request
     * @return array
     */
    public function index(Request $request)
    {
        if(!$request->has('movie_id')) {
            return $this->error(1, 'No found movie_id');
        }
        $movieId= $request->input('movie_id'); # 影视id
        $page = $request->has('page') ? $request->input('page') : 1; # 当前页数
        $limit = $request->has('limit') ? $request->input('limit') : 10; # 每页显示条数
        $data = MovieCommentDB::getCommentList($movieId, $page, $limit);
        $respons = [];
        $respons['data'] = $data;
        return $this->success($respons);
    }

    /**
     *
     * 影视评论发布
     *
     * @param Request $request
     * @return array
     */
    public function store(Request $request)
    {
        if(!$request->has('movie_id')) {
            return $this->error(1, 'No found movie_id');
        }
        $movieId = $request->input('movie_id'); # 影视id
        if(!$request->has('member_id')) {
            return $this->error(2, 'No found member_id');
        }
        $memberId = $request->input('member_id'); # 用户id
        if(!$request->has('content')) {
            return $this->error(3, 'No found comment content');
        }
        $content = $request->input('content'); # 评论内容
        # 查看该用户5分钟之内的评论有多少条
        $beforeTime = currentTime('Y-m-d H:i:s', (time() - 300));
        $count = MovieComment::where('member_id', $memberId)->where('create_time', '>', $beforeTime)->count();
        # 如果五分钟内超过5次评论，提示暂时不能评论
        if($count >= 5) {
            return $this->error(4, 'Your comments are too frequent!');
        }
        $result = MovieComment::setComment($movieId, $memberId, $content);
        if(!$result) {
            return $this->error(5, 'Comment fail!');
        }
        $respons = [];
        $respons['movie_id'] = $movieId;
        return $this->success($respons);
    }

}
