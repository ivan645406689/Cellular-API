<?php

namespace App\Http\Controllers\Movie;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class MovieCallBackController extends Controller
{
    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        //
    }

    /**
     *
     * 观影券购买回调
     *
     * @param Request $request
     * @return array
     */
    public function coupon(Request $request)
    {
        $respons = [];
        $respons['data'] = '观影券购买回调';
        return $this->success($respons);
    }

    /**
     *
     * VIP购买回调
     *
     * @param Request $request
     * @return array
     */
    public function vip(Request $request)
    {
        $respons = [];
        $respons['data'] = 'VIP购买回调';
        return $this->success($respons);
    }

}
