<?php

namespace App\Http\Controllers\Storage;

use App\Http\Controllers\Controller;
use OSS\OssClient;
use OSS\Core\OssException;

class OSSBucketController extends Controller
{
    private $client;

    /**
     * Create a new controller instance.
     */
    public function __construct()
    {
        $config = config('oss');
        try {
            $this->client = new OssClient($config['access_key_id'], $config['access_key_secret'], $config['endpoint']);
            # $this->client->setTimeout($config['time_out']);
            # $this->client->setConnectTimeout($config['connect_time_out']);
        } catch (OssException $e) {
            return $this->error('1001', ['msg' => $e->getMessage()]);
        }
    }

    public function index() {
        try{
            $bucketListInfo = $this->client->listBuckets();
        } catch(OssException $e) {
            return $this->error('1002', ['msg' => $e->getMessage()]);
        }
        $bucketList = $bucketListInfo->getBucketList();
        $response = [];
        foreach($bucketList as $bucket) {
            $response[] = [
                'a' => $bucket->getLocation(),
                'name' => $bucket->getName(),
                'create_date' => $bucket->getCreatedate()
            ];
        }
        return $this->success($response);
    }
}
