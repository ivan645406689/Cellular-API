<?php
namespace App\DB;

class CommonDB
{
    /**
     * 根据页码和显示记录数计算查询其实记录节点
     *
     * @param $result
     * @param $page
     * @param $limit
     * @return int|string
     */
    public static function page($result, $page, $limit)
    {
        if(!is_numeric($page) || empty($page)) $page = 1;
        $start = ($page - 1) * $limit;
        $arr = $result;
        $count = $arr->count();
        $pageCount = ($count % $limit == 0) ? ($count / $limit) : (int)($count / $limit) + 1;
        $result = $result->skip($start)->take($limit)->get();
        if($result != null) {
            $result = $result->toArray();
        }
        $data = [
            'data' => $result,
            'count' => $count,
            'page_count' => $pageCount,
        ];
        return $data;
    }

    /**
     * 根据页码和显示记录数计算查询其实记录节点
     *
     * @param $result
     * @param $page
     * @param $limit
     * @return int|string
     */
    public static function lists($result, $groupColunm, $getColum = null)
    {
        if($getColum == null) {
            $getColum = $groupColunm;
        }
        $result = $result->groupBy($groupColunm)->get();
        $data = [];
        foreach ($result as $val) {
            if(!in_array($val->$getColum, $data)) {
                $data[] = $val->$getColum;
            }
        }
        return $data;
    }
}