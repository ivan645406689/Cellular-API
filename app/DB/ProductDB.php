<?php

namespace App\DB;

use Illuminate\Support\Facades\DB;

class ProductDB
{
    /**
     *
     * 查看商品
     *
     * @param $product_id
     *
     * @return bool
     */
    public static function getProduct($productId)
    {
        $product = DB::table('product as p')->select('p.id', 'p.category_id', 'p.create_time', 'p.title', 'p.price', 'p.sale_price', 'p.amount',
            'p.virtual_callback', 'p.is_virtual', 'p.status','p.thumb','pc.name')
            ->leftJoin('product_category as pc', 'pc.id', 'p.category_id')
            ->where('p.id', $productId)
            ->where('p.is_delete', 0)->get();
        $data = ($product) ? $product : null;
        return $data;
    }

    /**
     *
     * 商品列表
     *
     * @param
     * $status
     * $is_virtual
     * $page
     * $limit
     * @return bool
     */
    public static function getProductList($status, $isVirtual, $page, $limit)
    {
        $result = DB::table('product as p')->select('p.id', 'p.category_id', 'p.create_time', 'p.title', 'p.price', 'p.sale_price', 'p.amount',
            'p.virtual_callback', 'p.is_virtual', 'p.status', 'p.thumb', 'pc.name')
            ->leftJoin('product_category as pc', 'pc.id', 'p.category_id')
            ->where('p.status', $status)
            ->where('p.is_delete', 0)
            ->where('p.is_virtual', $isVirtual)
            ->orderBy('p.rank', 'desc');
        $data = CommonDB::page($result, $page, $limit);
        return $data;
    }
}