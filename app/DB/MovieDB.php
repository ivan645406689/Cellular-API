<?php

namespace App\DB;

use Illuminate\Support\Facades\DB;

class MovieDB
{

    /**
     * 查询相关影视ID
     *
     * @param $id
     * @param $start
     * @param $limit
     * @return mixed
     */
    public static function RecommendMovie($id, $start, $limit)
    {
        $result = DB::table(DB::raw('c_movie_map_category as mc'))
            ->select(DB::raw('distinct movie_id'))
            ->whereExists(
                function ($query) use ($id) {
                    $query->select(DB::raw('distinct id'))
                        ->from('movie_map_category')
                        ->where('movie_id', $id)
                        ->where('category_id', DB::raw('mc.category_id'));
                }
            )
            ->skip($start)
            ->take($limit)
            ->get();
        $response = [];
        foreach ($result as $val) {
            $response[] = $val->movie_id;
        }
        return $response;
    }

    /**
     * 查询分类下的影视信息
     *
     * @param $categoryID
     * @param $page
     * @param $limit
     * @return mixed
     */
    public static function getMovieList($categoryID, $page, $limit)
    {
        if(is_numeric($categoryID) && !empty($categoryID)) {
            $result = DB::table('movie as m')->select('m.id', 'm.title', 'm.status', 'm.thumb', 'm.type')
                ->leftJoin('movie_map_category as map', 'map.movie_id', 'm.id')
                ->where('map.category_id', $categoryID)
                ->where('m.is_delete', 0);
        } else {
            $result = DB::table('movie')->select('id', 'title', 'status', 'thumb', 'type')->where('is_delete', 0);
        }
        if(!is_numeric($page) || empty($page)) $page = 1;
        $start = ($page - 1) * $limit;
        $movie = $result->skip($start)->take($limit)->get();
        $data = ($movie) ? $movie->toArray() : null;
        return $data;
    }

}
