<?php

namespace App\DB;

use App\Model\MovieVip;
use App\Model\Order;
use Illuminate\Support\Facades\DB;

class MovieVipDB
{

    /**
     *
     * 虚拟商品自动发货
     *
     * @param $mp
     * @param $id
     * @return bool
     */
    public static function autoDelivery($data)
    {
        # 获得订单ID
        $orderId = $data['order_id'];
        # 获取用户ID
        $memberId = $data['member_id'];
        # 获取商品类型
        $type = $data['type'];
        # 获取商品数值
        $amount= $data['amount'];
        # 事务开始
        DB::beginTransaction();
        # 获得订单用户的VIP记录
        $movieVip = MovieVip::where('member_id', $memberId)->first();
        # 如无VIP记录，则创建
        if(!$movieVip) {
            $movieVip = new MovieVip();
            $movieVip->member_id = $memberId;
        }
        # 判断影视商品类型，0为观影券，1为VIP
        switch($type) {
            case 0:
                # 观影券添加点数
                $movieVip->coupon = ($movieVip->coupon != null) ? $movieVip->coupon + $amount : $amount;
                break;
            case 1:
                # 如果用户VIP到期时间为空，或VIP到期时间早已在当前时间前（证明用户以前曾买过VIP，但现在已经过期），VIP时间为当前时间戳加上商品中的时间
                $vipTime = ($movieVip->vip_time != null) ? strtotime($movieVip->vip_time) : null;
                if($vipTime == null || $vipTime < time()) {
                    $vipTime = date('Y-m-d H:i:s', (time() + $amount));
                }
                else{
                    # 如果用户VIP到期时间在当前时间之后（证明当前用户还在VIP有效时间内，只是延续VIP时间），则VIP时间在当前用户的VIP到期时间后累加商品中的时间
                    $vipTime = date('Y-m-d H:i:s', ($vipTime + $amount));
                }
                $movieVip->vip_time = $vipTime;
                break;
            default:
                DB::rollBack();
                return false;
                break;
        }
        # 保存VIP记录的修改
        if(!$movieVip->save()) {
            DB::rollBack();
            return false;
        }
        # 订单完成
        $finalOrder = Order::orderFinal($orderId);
        if(!$finalOrder) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }
}