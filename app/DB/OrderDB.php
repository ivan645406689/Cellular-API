<?php

namespace App\DB;

use Illuminate\Support\Facades\DB;

class OrderDB
{

    /**
     *
     * 查看订单详情
     *
     * @param $order_id
     *
     * @return bool
     */
    public static function getOrder($id)
    {
        $order = DB::table('order as o')->select('o.product_id', 'o.id', 'o.create_time', 'o.total', 'o.price', 'o.pay_price', 'o.pay_status',
            'o.virtual_callback', 'o.is_virtual', 'o.status', 'm.username', 'm.mobile', 'p.title', 'p.price')
            ->leftJoin('member as m', 'm.id', 'o.member_id')
            ->leftJoin('product as p', 'p.id', 'o.product_id')
            ->where('o.id', $id)->get();
        $data = ($order) ? $order : null;
        return $data;
    }


    /**
     *
     * 查看会员订单
     *
     * @param $member_id
     *
     * @return bool
     */
    public static function getMemberOrderList($memberId, $page, $limit)
    {
        $result = DB::table('order as o')->select('o.id', 'o.product_id', 'o.create_time', 'o.total', 'o.price', 'o.pay_price', 'o.pay_status',
            'o.virtual_callback', 'o.is_virtual', 'o.status','m.username','m.mobile','p.title','p.price')
            ->leftJoin('member as m', 'm.id', 'o.member_id')
            ->leftJoin('product as p', 'p.id', 'o.product_id')
            ->where('o.member_id', $memberId)
            ->orderBy('o.rank', 'desc');
        $data = CommonDB::page($result, $page, $limit);
        return $data;
    }

}