<?php

namespace App\DB;

use Illuminate\Support\Facades\DB;

class MovieCommentDB
{
    /**
     *
     * 获取影视评论列表
     *
     * @param $movieId
     * @param $page
     * @param $limit
     * @return array
     */
    public static function getCommentList($movieId, $page, $limit)
    {
        # 查询评论记录
        $commentList = DB::table('movie_comment as mc')->select('mc.id', 'mc.member_id', 'mc.content', 'mc.parent_id', 'm.username', 'm.nickname', 'm.avatar', 'mc.create_time')
            ->leftJoin('member as m',  'm.id', '=' , 'mc.member_id')
            ->where('mc.movie_id', $movieId);
        $commentList = CommonDB::page($commentList, $page, $limit);
        return $commentList;
    }
}