<?php

namespace App\DB;

use App\Model\WechatGameSn;
use App\Model\WechatMpGameAward;
use Illuminate\Support\Facades\DB;

class WechatMpGameDB
{

    /**
     *
     * 根据公众账号ID，获取游戏列表
     *
     * @param $id
     * @return array|bool
     */
    public static function getMpGameList($wechatMpId, $page, $limit)
    {
        $result = DB::table('wechat_mp_game as wmg')
            ->select(
                'wmg.id', 'wmg.create_time', 'wmg.game_id', 'wmg.title', 'wmg.win_where', 'wmg.win_value', 'wmg.win_odds', 'wmg.share_amount', 'wmg.share_use_amount',
                'g.name', 'g.resource', 'g.type'
            )
            ->leftJoin('game as g', 'wmg.game_id', '=', 'g.id')
            ->where('wmg.wechat_mp_id', $wechatMpId)
            ->where('wmg.status', 1);
        $mpGameList = CommonDB::page($result, $page, $limit);
        return $mpGameList;
    }


    /**
     *
     * 操作游戏胜利数据
     *
     * @param $data
     * @param $type
     * @return bool
     */
    public static function getWinCode($data)
    {
        # 随即抽取获奖商品
        # 读取微信活动商品表
        $wechatMpGameAwardList = WechatMpGameAward::where('wechat_mp_game_id', $data['wechat_mp_game_id'])->where('amount', '>', 0)->where('status', 1)->where('is_delete', 0)->get();
        if(!$wechatMpGameAwardList) {
            return false;
        }
        $wechatMpGameAwardList = $wechatMpGameAwardList->toArray();
        $count = count($wechatMpGameAwardList);
        $rand = rand(0, $count - 1);
        $luckAward = $wechatMpGameAwardList[$rand];
        # 随机生成获奖码Code
        $uniqid = uniqid();
        $uniqid = substr($uniqid, (strlen($uniqid) - 6));
        $time = time();
        $time = substr($time, 8);
        $code = strtoupper($uniqid . $time);
        if($luckAward['type'] == 0) {
            # 添加活动商品发放记录
            $dataInsert = [
                'wechat_id' =>  $data['wechat_id'],
                'wechat_mp_id' => $data['wechat_mp_id'],
                'wechat_mp_game_id' => $data['wechat_mp_game_id'],
                'award_type' => $luckAward['type'],
                'award_id' => $luckAward['product_id'],
                'openid' => $data['openid'],
                'code' => $code,
                'status' => 0,
            ];
            $result = WechatGameSn::insert($dataInsert);
            if(!$result) {
                return false;
            }
        }
        elseif ($luckAward['type'] == 1){
            # 添加活动商品发放记录
            $dataInsert = [
                'wechat_id' =>  $data['wechat_id'],
                'wechat_mp_id' => $data['wechat_mp_id'],
                'wechat_mp_game_id' => $data['wechat_mp_game_id'],
                'award_type' => $luckAward['type'],
                'award_id' => $luckAward['wechat_mp_redpack_id'],
                'openid' => $data['openid'],
                'code' => $code,
                'status' => 0,
            ];
            $result = WechatGameSn::insert($dataInsert);
            if(!$result) {
                return false;
            }
        }
        else {
            return false;
        }
        return $code;
    }
}