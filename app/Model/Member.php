<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Hash;

class Member extends Model
{
    protected $table = 'member';
    public $timestamps = false;

    /**
     *
     * 获取用户列表
     *
     * @param $page
     * @param $limit
     * @return bool|int|string
     */
    public static function getMemberList($page, $limit)
    {
        $result = self::select('id', 'username', 'mobile', 'nickname', 'avatar');
        $memberList = Common::page($result, $page, $limit);
        if($memberList) {
            return $memberList;
        }
        return false;
    }

    /**
     *
     * 获取用户公开信息
     *
     * @param $id
     * @return bool
     */
    public static function getShowMember($id)
    {
        $member = self::select('id', 'username', 'email', 'mobile', 'nickname', 'sex', 'avatar')
            ->find($id);
        if($member) {
            return $member->toArray();
        }
        return false;
    }

    /**
     *
     * 获取用户详细信息
     *
     * @param $id
     * @return bool
     */
    public static function getMemberDetail($id)
    {
        $member = self::select('id', 'username', 'email', 'mobile', 'nickname', 'name', 'sex', 'birthday', 'province', 'city', 'district', 'address', 'avatar', 'money')
            ->find($id);
        if($member) {
            return $member->toArray();
        }
        return false;
    }

    /**
     *
     * 获取用户详细信息
     *
     * @param $id
     * @return bool
     */
    public static function updateMember($id, $data)
    {
        $member = self::find($id);
        $member->email = $data->email;
        $member->nickname = (isset($data->nickname)) ? $data->nickname : $member->username;
        $member->name = $data->name;
        $member->sex = $data->sex;
        $member->birthday = $data->birthday;
        $member->province = (isset($data->province)) ? $data->province : $member->province;
        $member->city = (isset($data->city)) ? $data->city : $member->city;
        $member->district = (isset($data->district)) ? $data->district : $member->district;
        $member->address = (isset($data->address)) ? $data->address : $member->address;
        if($member->save()) {
            return true;
        }
        return false;
    }

    /**
     *
     * 修改密码
     *
     * @param $id
     * @param $original
     * @param $password
     * @return bool
     */
    public static function updatePassword($id, $original, $password)
    {
        $member = self::find($id);
        if(Hash::make($original) != $member->password) {
            return false;
        }
        $password = Hash::make($password);
        $member->password = $password;
        if(!$member->save()) {
            return false;
        }
        return true;
    }
}
