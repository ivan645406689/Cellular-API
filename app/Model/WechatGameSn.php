<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WechatGameSn extends Model
{
    protected $table = 'wechat_game_sn';
    public $timestamps = false;
}