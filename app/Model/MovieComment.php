<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class MovieComment extends Model
{
    protected $table = 'movie_comment';
    public $timestamps = false;

    /**
     *
     * 发表评论
     *
     * @param $movieId
     * @param $memberId
     * @param $content
     * @return bool
     */
    public static function setComment($movieId, $memberId, $content)
    {
        $data = [
            'member_id' => $memberId,
            'movie_id' => $movieId,
            'content' => $content,
        ];
        $result = self::insert($data);
        if($result) {
            return true;
        }
        return false;
    }
}