<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WechatMp extends Model
{
    protected $table = 'wechat_mp';
    public $timestamps = false;
}