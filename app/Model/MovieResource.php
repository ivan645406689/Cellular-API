<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MovieResource extends Model
{

    protected $table = 'movie_resource';
    public $timestamps = false;

    /**
     *
     * 获取电影详细信息
     *
     * @param $id
     * @return array|bool
     */
    public static function verCanSee($id, $memberId)
    {
        # 查看当前用户是否为VIP
        $movieVip = MovieVip::where('member_id', $memberId)->first();
        if(!$movieVip) {
            return false;
        }
        # 如果当前vip权限未过期，直接返回“该资源可看”
        if($movieVip->vip_time != null && $movieVip->vip_time > currentTime()){
            return true;
        }
        # 查看用户是否使用过观影券购买该资源
        $movieBuyLog = MovieBuyLog::where('movie_resource_id', $id)->where('member_id', $memberId)->first();
        if($movieBuyLog) {
            return true;
        }
        return false;
    }

    /**
     * 
     * 添加播放记录
     * 
     * @param $id
     * @param $memberId
     */
    public static function playResource($id, $memberId)
    {
        $data = [
            'member_id' => $memberId,
            'movie_resource_id' => $id
        ];
        $result = MoviePlayLog::insert($data);
        if($result) {
            return true;
        }
        return false;
    }
}