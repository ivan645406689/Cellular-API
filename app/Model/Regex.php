<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Regex extends Model
{
    protected $table = 'regex';
    public $timestamps = false;

    /**
     * 获取正则表达式
     *
     * @return array
     */
    public static function keyName()
    {
        $regex = array();
        $result = self::get();
        foreach ($result as $key => $val) {
            $regex[$val->name] = $val->expression;
        }
        return $regex;
    }
}