<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class ProductCategory extends Model
{
    protected $table = 'product_category';
    public $timestamps = false;

    /**
     *
     * 获取所有分类
     *
     * @return mixed
     */
    public static function getProductCategory($parent_id)
    {
        $category = self::select('id', 'name')->where('parent_id', $parent_id)->orderBy('rank')->get();
        return $category;
    }
}