<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MovieBuyLog extends Model
{
    protected $table = 'movie_buy_log';
    public $timestamps = false;
    
}