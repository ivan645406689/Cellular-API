<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class WechatReply extends Model
{
    protected $table = 'wechat_reply';
    public $timestamps = false;

    public static function getReply($receive)
    {
        $result = self::select('reply')->where('receive', $receive)->first();
        if (!empty($result->reply)) return $result->reply;
        return null;
    }
}
