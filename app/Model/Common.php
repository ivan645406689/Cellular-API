<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Common extends Model
{

    /**
     * 根据页码和显示记录数计算查询其实记录节点
     *
     * @param $result
     * @param $page
     * @param $limit
     * @return int|string
     */
    protected static function page($result, $page, $limit)
    {
        if(!is_numeric($page) || empty($page)) $page = 1;
        $start = ($page - 1) * $limit;
        $arr = $result;
        $count = $arr->count();
        $pageCount = ($count % $limit == 0) ? ($count / $limit) : (int)($count / $limit) + 1;
        $result = $result->skip($start)->take($limit)->get();
        if($result != null) {
            $result = $result->toArray();
        }
        $data = [
            'data' => $result,
            'count' => $count,
            'page_count' => $pageCount,
        ];
        return $data;
    }

    /**
     * 根据页码和显示记录数计算查询其实记录节点
     *
     * @param $result
     * @param $page
     * @param $limit
     * @return int|string
     */
    protected static function lists($result, $colunm)
    {
        $result = $result->groupBy($colunm)->get();
        $data = [];
        foreach ($result as $val) {
            if(!in_array($val[$colunm], $data)) {
                $data[] = $val[$colunm];
            }
        }
        return $data;
    }

}
