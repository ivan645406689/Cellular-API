<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

use App\DB\MovieDB;

class Movie extends Model
{

    protected $table = 'movie';
    public $timestamps = false;

    /**
     *
     * 获取电影详细信息
     *
     * @param $id
     * @return array|bool
     */
    public static function getMovie($id)
    {
        $movie = self::select('id', 'title', 'thumb', 'status')->where('is_delete', 0)->find($id);
        if(!$movie) {
            return false;
        }
        $movieResource = MovieResource::select('id', 'rank', 'movie_id', 'type', 'title', 'click', 'price', 'is_new', 'is_hot',  'path', 'status')
            ->where('is_delete', 0)->get();
        if($movieResource) {
            $movieResource = $movieResource->toArray();
        }
        else{
            $movieResource = null;
        }
        $data = [
            'movie' => $movie,
            'resource' => $movieResource
        ];
        return $data;
    }

    /**
     *
     * 获取与当前影视相同分类的前五个影视（推荐影视）
     *
     * @param $id
     * @return bool
     */
    public static function getRecommendList($id)
    {
        $movieIds = MovieDB::RecommendMovie($id, 0, 5);
        $movie = self::select('id', 'title', 'thumb', 'status')->whereIn('id', $movieIds)->where('id', '<>', $id)->where('is_delete', 0)->get()->toArray();
        return $movie;
    }

}
