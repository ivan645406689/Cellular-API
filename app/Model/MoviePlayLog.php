<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MoviePlayLog extends Model
{
    protected $table = 'movie_play_log';
    public $timestamps = false;
    
}