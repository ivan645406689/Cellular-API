<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'order';
    public $timestamps = false;

    /**
     *
     * 下单操作
     *
     * @param $info
     *
     * @return bool
     */
    public static function createOrder($info)
    {
        $order = new self();
        $order->member_id = $info['member_id'];
        $order->product_id = $info['product_id'];
        $order->total = $info['total'];
        $order->price = $info['price'];
        $order->pay_status = 0;
        $order->virtual_callback = $info['virtual_callback'];
        $order->is_virtual = $info['is_virtual'];
        $order->status = 0;
        if(!$order->save()) {
            return false;
        }else{
            return  $order->id;
        }
    }

    /**
     * 
     * 虚拟商品自动发货
     * 
     * @param $mp
     * @param $id
     * @return bool
     */
    public static function orderFinal($id)
    {
        # 获得订单
        $order = self::find($id);
        if(!$order) {
            return false;
        }
        # 订单完成
        $order->status = 1;
        if(!$order->save()) {
            return false;
        }
        return true;
    }
}