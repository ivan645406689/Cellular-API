<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class AD extends Model
{
    protected $table = 'ad';
    public $timestamps = false;
}