<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MovieCategory extends Model
{
    protected $table = 'movie_category';
    public $timestamps = false;

    /**
     *
     * 获取所有分类
     *
     * @return mixed
     */
    public static function getMovieCategory()
    {
        $category = self::select('id', 'name')->where('level', 1)->orderBy('create_time')->limit(10)->get();
        return $category;
    }
}
