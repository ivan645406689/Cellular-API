<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class Wechat extends Model
{
    protected $table = 'wechat';
    public $timestamps = false;
}
