<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WechatGame extends Model
{
    protected $table = 'wechat_game';
    public $timestamps = false;

    /**
     *
     * 创建微信用户、游戏关联
     *
     * @return mixed
     */
    public static function createWechatGame($data)
    {
        $wechatGame = new self();
        $wechatGame->wechat_id = $data['wechat_id'];
        $wechatGame->wechat_mp_id = $data['wechat_mp_id'];
        $wechatGame->wechat_mp_game_id = $data['wechat_mp_game_id'];
        $wechatGame->last_time = currentTime();
        if(!$wechatGame->save()) {
            return false;
        }
        return $wechatGame;
    }

    /**
     *
     * 清空当日参与数
     *
     * @param $id
     */
    public static function clearDayTotal($id)
    {
        $wechatGame = self::find($id);
        $wechatGame->day_total = 0;
        $wechatGame->last_time = currentTime();
        if(!$wechatGame->save()) {
            return false;
        }
        return $wechatGame;
    }

    /**
     *
     * 记录玩儿游戏次数
     *
     * @param $id
     */
    public static function recordTimes($id)
    {
        $wechatGame = self::find($id);
        $wechatGame->day_total += 1;
        $wechatGame->total += 1;
        $wechatGame->last_time = currentTime();
        if(!$wechatGame->save()) {
            return false;
        }
        return true;
    }
}