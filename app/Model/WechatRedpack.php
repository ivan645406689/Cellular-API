<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;

class WechatRedpack extends Model
{
    protected $table = 'wechat_redpack';
    public $timestamps = false;
}