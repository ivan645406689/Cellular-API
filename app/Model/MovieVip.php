<?php

namespace App\Model;
use Illuminate\Database\Eloquent\Model;

class MovieVip extends Model
{
    protected $table = 'movie_vip';
    public $timestamps = false;

}