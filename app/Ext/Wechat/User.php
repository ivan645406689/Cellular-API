<?php

namespace App\Ext\Wechat;

class User
{
    /**
     * 获取用户基本信息（包括UnionID机制）
     * @param string $token 调用接口凭证
     * @param string $openID 普通用户的标识，对当前公众号唯一
     * @param string $lang 返回国家地区语言版本，zh_CN 简体，zh_TW 繁体，en 英语
     * @return bool
     */
    public static function unionID($token, $openID, $lang = 'zh-CN')
    {
        if (empty($token)) die('token is not defined');
        $url = 'https://api.weixin.qq.com/cgi-bin/user/info?access_token=' . $token . '&openid=' . $openID . '&lang=' . $lang;
        $callback = Common::get($url);
        $callback = json_decode($callback);
        if (empty($callback->errcode)) {
            return $callback;
        } else {
            die('wechat error: [' . $callback->errcode . '] ' . $callback->errmsg);
        }
        return false;
    }
}