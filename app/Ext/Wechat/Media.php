<?php
/**
 * 微信自定义菜单接口
 */

namespace App\Ext\Wechat;

use App\Ext\Wechat\Common;
use Exception;

class Media
{

    private static $mediaType = ['image', 'video', 'voice', 'news'];

    /**
     * 获取媒体素材列表
     *
     * @param $token
     * @param $type
     * @param int $page
     * @param int $limit
     * @return bool
     */
    public static function get($token, $type, $page = 1, $limit = 20)
    {
        if (empty($type) || !in_array($type, self::$mediaType)) {
            throw new Exception('type param is not defined!');
        }
        $param = [
            'type' => $type,
            'offset' => ($page - 1) * $limit,
            'count' => $limit,
        ];
        $url = 'https://api.weixin.qq.com/cgi-bin/material/batchget_material?access_token=' . $token;
        $response = Common::postJson($url, $param);
        return $response ? json_decode($response, true) : [];
    }

    /**
     * 获取媒体素材总数量
     *
     * @param $token
     * @return mixed
     */
    public static function count($token)
    {
        $url = 'https://api.weixin.qq.com/cgi-bin/material/get_materialcount?access_token=' . $token;
        $callback = Common::get($url);
        return json_decode($callback, true);
    }

}